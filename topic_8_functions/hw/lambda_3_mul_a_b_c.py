"""
lambda перемножает аргументы a, b и c и выводит результат
"""

mult = lambda a, b, c: print(a * b * c)

if __name__ == '__main__':
    mult(5, 2, 6)
def flower_with_vals(flower='ромашка', color='белый', price=10.25):
    """
    Функция flower_with_vals.

    Принимает 3 аргумента:
    цветок (по умолчанию "ромашка"),
    цвет (по умолчанию "белый"),
    цена (по умолчанию 10.25).

    Функция flower_with_default_vals выводит строку в формате
    "Цветок: <цветок> | Цвет: <цвет> | Цена: <цена>".

    При этом в функции flower_with_default_vals есть проверка на то, что цветок и цвет - это строки

    (* Подсказка: type(x) == str или isinstance(s, str)), а также цена - это число больше 0, но меньше 10000.
    В функции main вызвать функцию flower_with_default_vals различными способами
    (перебрать варианты: цветок, цвет, цена, цветок цвет, цветок цена, цвет цена, цветок цвет цена).

    (* Использовать именованные аргументы).
    """
    if type(flower) == str and type(color) == str and (0 < price < 1000):
        print(f'Цветок: <{flower}> | Цвет: <{color}> | Цена: <{price}> ')


if __name__ == '__main__':
    flower_with_vals()
    flower_with_vals('орхидея', 'белый', 199.99)
    flower_with_vals("лопух")
    flower_with_vals(color="желтый")
    flower_with_vals(price=5)

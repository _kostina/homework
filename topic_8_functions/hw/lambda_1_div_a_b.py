"""
lambda делит аргумент a на аргумент b и выводит результат
"""

division = lambda a, b: print(a / b)

if __name__ == '__main__':
    division(10, 5)

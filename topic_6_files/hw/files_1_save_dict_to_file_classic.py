def save_dict_to_file_classic(path_str, save_dict):
    """
    Функция save_dict_to_file_classic.

    Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

    Сохраняет список в файл. Проверить, что записалось в файл.
    """
    with open(path_str, 'w') as f:
        f.write(str(save_dict))


if __name__ == '__main__':
    test_dict = {'key': 1, 'key2': 2}
    save_dict_to_file_classic('text1.txt', test_dict)



import pickle
def save_dict_to_file_pickle(path_str, save_dict):
    """
    Функция save_dict_to_file_pickle.
    
    Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).
    
    Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
    """
    with open(path_str, 'wb') as file:
        pickle.dump(save_dict, file)


if __name__ == '__main__':
    save_dict_to_file_pickle('text2.txt', {'key': 'x'})
import json

def read_str_from_file(path_name: str):
    """
    Функция read_str_from_file.

    Принимает 1 аргумент: строка (название файла или полный путь к файлу).

    Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и наполнен каким-то текстом.)
    """
    with open(path_name, 'r') as file:
        loaded_dict = json.load(file)

    print(str(loaded_dict))

if __name__ == '__main__':
    read_str_from_file('test3.json')
from topic_7_oop.hw.class_3_1_chicken import Chicken
from topic_7_oop.hw.class_3_2_goat import Goat

class Farm:
    """
    Класс Farm.

    Поля:
    животные (list из произвольного количества Goat и Chicken): zoo_animals
    (вначале список пуст, потом добавялем животных методами append и extend самостоятельно),
    наименование фермы: name,
    имя владельца фермы: owner.

    Методы:
    get_goat_count: вернуть количество коз на ферме (*Подсказка isinstance или type == Goat),
    get_chicken_count: вернуть количество куриц на ферме,
    get_animals_count: вернуть количество животных на ферме,
    get_milk_count: вернуть сколько молока можно получить в день,
    get_eggs_count: вернуть сколько яиц можно получить в день.
    """
    def __init__(self, name, owner):
        self.name = name
        self.owner = owner
        self.zoo_animals = []

    # def add_goat(self):
    #     self.zoo_animals.append(Goat)
    #
    # def add_chicken(self):
    #     self.zoo_animals.append(Chicken)

    def get_goat_count(self):
        goat = 0
        for i in self.zoo_animals:
            if type(i) == Goat:
                goat += 1
        return goat

    def get_chicken_count(self):
        chicken = 0
        for i in self.zoo_animals:
            if type(i) == Chicken:
                chicken += 1
        return chicken

    def get_animals_count(self):
        return len(self.zoo_animals)

    def get_milk_count(self):
        milk = 0
        for i in self.zoo_animals:
            if type(i) == Goat:
                milk += i.milk_per_day
        return milk

    def get_eggs_count(self):
        egg = 0
        for i in self.zoo_animals:
            if type(i) == Chicken:
                egg += i.eggs_per_day
        return egg

from topic_7_oop.hw.class_4_1_pupil import Pupil
from topic_7_oop.hw.class_4_2_worker import Worker


def count_avg(arr):
    return sum(arr)/len(arr)


class School:
    """
    Класс School.

    Поля:
    список людей в школе (общий list для Pupil и Worker): people,
    номер школы: number.

    Методы:
   +get_avg_mark: вернуть средний балл всех учеников школы
   +get_avg_salary: вернуть среднюю зп работников школы
   +get_worker_count: вернуть сколько всего работников в школе
   +get_pupil_count: вернуть сколько всего учеников в школе
    +get_pupil_names: вернуть все имена учеников (с повторами, если есть)
    +get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
    +get_max_pupil_age: вернуть возраст самого старшего ученика
    +get_min_worker_salary: вернуть самую маленькую зп работника
    get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
    (список из одного или нескольких элементов)
    """
    def __init__(self, people: list, number):
        self.people = people
        self.number = number

    def get_pupil_list(self):
        return [x for x in self.people if type(x) == Pupil]

    def get_worker_list(self):
        return [y for y in self.people if type(y) == Worker]

    def get_avg_mark(self):
        pupils = self.get_pupil_list()
        # marks = list(map(lambda x: x.get_avg_mark(), pupils))
        marks = []
        for i in pupils:
            marks.append(i.get_avg_mark())
        return count_avg(marks)

    def get_avg_salary(self):
        workers = self.get_worker_list()
        salaries = []
        for i in workers:
            salaries.append(i.salary)
        return count_avg(salaries)

    def get_worker_count(self):
        return len(self.get_worker_list())

    def get_pupil_count(self):
        return len(self.get_pupil_list())

    def get_pupil_names(self):
        # names = []
        # for i in self.get_pupil_list():
        #     names.append(i.name)
        return [i.name for i in self.get_pupil_list()]

    def get_unique_worker_positions(self):
        # positions = set()
        # for i in self.get_worker_list():
        #     positions.add(i.position)
        return {i.position for i in self.get_worker_list()}

    def get_max_pupil_age(self):
        age = 0
        for i in self.get_pupil_list():
            if age < i.age:
                age = i.age
        return age

    def get_min_worker_salary(self):
        workers = self.get_worker_list()
        min_salary = workers[0].salary
        for i in workers:
            if min_salary > i.salary:
                min_salary = i.salary
        return min_salary

    def get_min_salary_worker_names(self):
        workers = self.get_worker_list()
        min_salary = self.get_min_worker_salary()
        return [i.name for i in workers if i.salary <= min_salary]






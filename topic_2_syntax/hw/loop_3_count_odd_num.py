def count_odd_num(x):
    """
    Функция count_odd_num.
    
    Принимает натуральное число (целое число > 0).
    Верните количество нечетных цифр в этом числе.
    Если число равно 0, то вернуть "Must be > 0!".
    Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
    """
    if type(x) != int:
        return "Must be int!"

    if x <= 0:
        return "Must be > 0!"

    c = 0
    for i in str(x):
        if int(i) % 2 == 1:
            c += 1
    return c
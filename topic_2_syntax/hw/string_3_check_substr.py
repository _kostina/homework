def check_substr(first_str, second_str):
    """
    Функция check_substr.

    Принимает две строки.
    Если меньшая по длине строка содержится в большей, то возвращает True,
    иначе False.
    Если строки равны, то False.
    Если одна из строк пустая, то True.
    """
    if len(first_str) < len(second_str) and first_str in second_str:
        return True

    elif len(first_str) > len(second_str) and second_str in first_str:
        return True

    elif len(first_str) == len(second_str):
        return False

    else:
        return False




def print_hi(n):
    """
    Функция print_hi.

    Принимает число n.
    Выведите на экран n раз фразу "Hi, friend!"
    Пример: n=3, тогда в результате "Hi, friend!Hi, friend!Hi, friend!"
    """
    base_str = "Hi, friend!"
    if n > 0:
        res = ''
        for i in range(n):
            res += base_str
        print(res)
    else:
        print('')

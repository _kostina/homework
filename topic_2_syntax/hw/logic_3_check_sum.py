def check_sum(a, b, c):
    """
    Функция check_sum.

    Принимает 3 числа.
    Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
    """
    if a + b == c or a + c == b or c + b == a:
        return True
    else:
        return False
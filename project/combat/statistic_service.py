import psycopg2

from project.combat.game_result import GameResult


class SQLHelper:

    def __init__(self):
        self.conn = psycopg2.connect(host='localhost',
                                     database='naruto-bot',
                                     user='postgres',
                                     password='root')
        self.cursor = self.conn.cursor()
        init_query = """
        create table if not exists naruto_stat(
              id bigint primary key,
              win int,
              lose int,
              equal int
        );"""
        self.cursor.execute(init_query)
        self.conn.commit()

    def get_stat(self, user_id):
        query = f'SELECT win, lose, equal FROM naruto_stat WHERE id = {user_id};'
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        if result is None:
            zero_stat = [0, 0, 0]
            insert_query = 'INSERT INTO naruto_stat (id, win, lose, equal) VALUES ({},{},{},{})' \
                .format(user_id, *zero_stat)
            self.cursor.execute(insert_query)
            self.conn.commit()
            return zero_stat
        else:
            stat = [*result]
            if len(stat) != 3:
                raise IndexError("Длина stat не равна 3!")
            return stat

    def update_stat(self, user_id, result: GameResult):
        print("Обновление статистики", end="...")
        stat = self.get_stat(user_id)
        if len(stat) != 3:
            raise IndexError("Длина stat не равна 3!")

        if result == GameResult.W:
            stat[0] = stat[0] + 1
        elif result == GameResult.L:
            stat[1] = stat[1] + 1
        elif result == GameResult.E:
            stat[2] = stat[2] + 1
        else:
            print(f"Не существует результата {result}")

        query = 'UPDATE naruto_stat ' \
                'SET win = {}, lose = {}, equal = {} ' \
                'WHERE id = {};'.format(*stat[:4], user_id)

        self.cursor.execute(query)
        self.conn.commit()
        print('Резултат обновлён')


if __name__ == '__main__':
    helper = SQLHelper()
    print(helper.get_stat("1"))
    helper.update_stat("1", GameResult.E)
    helper.update_stat("1", GameResult.E)
    helper.update_stat("1", GameResult.E)
    print(helper.get_stat("1"))

from enum import Enum

# победа, проигрыш и ничья
# tie - ничья

GameResult = Enum("GameResult", "W L E")

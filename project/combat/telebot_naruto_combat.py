import telebot

from telebot import types

from project.combat.body_part import BodyPart
from project.combat.game_result import GameResult
from project.combat.naruto import Naruto
from project.combat.naruto_npc import NarutoNPC
from project.combat.naruto_by_type import naruto_by_type
from project.combat.naruto_status import NarutoStatus
from project.combat.naruto_type import NarutoType
from project.combat.statistic_service import SQLHelper

with open("./bot_token.txt", 'r') as token_file:
    token = token_file.read().strip()

bot = telebot.TeleBot(token)

status = {}

body_part_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                               one_time_keyboard=True,
                                               row_width=len(BodyPart))

body_part_keyboard.row(*[types.KeyboardButton(body_part.name) for body_part in BodyPart])

statistic_service = SQLHelper()


@bot.message_handler(commands=["help", "info"])
def help_command(message):
    bot.send_message(message.chat.id, "Hi")


@bot.message_handler(commands=["start"])
def start(message):
    yes_no_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                one_time_keyboard=True,
                                                row_width=2)
    yes_no_keyboard.row("Да", "Нет")

    bot.send_message(message.from_user.id,
                     text="Готов приступить к схватке?",
                     reply_markup=yes_no_keyboard)

    bot.register_next_step_handler(message, start_question_handler)


@bot.message_handler(commands=["stat"])
def statistic(message):
    try:
        stat = statistic_service.get_stat(message.chat.id)
        result = f'Cтатистика игрока: \nПобед: {stat[0]}\nПроигрышей: {stat[1]}\nНичьих: {stat[2]}'
        bot.send_message(message.chat.id, result)
    except IndexError:
        bot.send_message(message.chat.id, "Не удалось получить статистику")


def start_question_handler(message):
    if message.text.lower() == 'да':
        bot.send_message(message.from_user.id,
                         "Начинаем!")
        created_npc(message)

        ask_about_naruto_type(message)

    elif message.text.lower() == 'нет':
        bot.send_message(message.from_user.id, 'Я подожду.')
    else:
        bot.send_message(message.from_user.id,
                         "Такой вариант мне не знаком.")


def created_npc(message):
    print(f"Начало создания объекта NPC для chat id = {message.chat.id}")
    global status
    naruto_npc = NarutoNPC()
    if status.get(message.chat.id, None) is None:
        status[message.chat.id] = {}
    status[message.chat.id]['npc_naruto'] = naruto_npc

    npc_image_filename = naruto_by_type[naruto_npc.naruto_type][naruto_npc.name]
    bot.send_message(message.chat.id, "Оппонент:")
    with open(f"../pictures/{npc_image_filename}", 'rb') as file:
        bot.send_photo(message.chat.id, file, naruto_npc)
    print(f"Завершение создания объекта NPC для chat id = {message.chat.id}")


def ask_about_naruto_type(message):
    print(f"Начало создания объекта Шиноби для chat id = {message.chat.id}")
    markup = types.InlineKeyboardMarkup()

    # TODO: несколько кнопок в ряду
    for naruto_type in NarutoType:
        markup.add(types.InlineKeyboardButton(text=naruto_type.name,
                                              callback_data=f"naruto_type_{naruto_type.value}"))
    bot.send_message(message.chat.id, "Выбери тип игрока:", reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "naruto_type_" in call.data)
def naruto_type_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) < 3 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, "Возникла проблема. Необходимо перезапустить сессию!")
    else:
        naruto_type_id = int(call_data_split[2])

        bot.send_message(call.message.chat.id, "Выбери своего бойца:")
        ask_user_about_naruto_by_type(naruto_type_id, call.message)


def ask_user_about_naruto_by_type(naruto_type_id, message):
    naruto_type = NarutoType(naruto_type_id)
    naruto_dict_by_type = naruto_by_type.get(naruto_type, {})

    for naruto_name, naruto_img in naruto_dict_by_type.items():
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton(text=naruto_name,
                                              callback_data=f"naruto_name_{naruto_type_id}_{naruto_name}"))
        with open(f"../pictures/{naruto_img}", 'rb') as file:
            bot.send_photo(message.chat.id, file, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "naruto_name_" in call.data)
def naruto_name_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) < 4 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, "Возникла проблема. Необходимо перезапустить сессию!")
    else:
        naruto_type_id, naruto_name = int(call_data_split[2]), call_data_split[3]

        create_user_naruto(call.message, naruto_type_id, naruto_name)

        bot.send_message(call.message.chat.id, "Игра началась!")

        game_next_step(call.message)


def create_user_naruto(message, naruto_type_id, naruto_name):
    global status
    user_naruto = Naruto(name=naruto_name,
                         naruto_type=NarutoType(naruto_type_id))

    if status.get(message.chat.id, None) is None:
        status[message.chat.id] = {}
    status[message.chat.id]['user_naruto'] = user_naruto

    image_filename = naruto_by_type[user_naruto.naruto_type][user_naruto.name]
    bot.send_message(message.chat.id, "Твой боец:")
    with open(f"../pictures/{image_filename}", 'rb') as file:
        bot.send_photo(message.chat.id, file, user_naruto)


def game_next_step(message):
    bot.send_message(message.chat.id,
                     "Выбор точки для защиты:",
                     reply_markup=body_part_keyboard)

    bot.register_next_step_handler(message, reply_defend)


def reply_defend(message):
    if not BodyPart.include_item(message.text):
        bot.send_message(message.chat.id, "Необходимо выбрать вариант на клавиатуре!")
        game_next_step(message)
    else:
        bot.send_message(message.chat.id,
                         "Выбор точки для удара:",
                         reply_markup=body_part_keyboard)

        bot.register_next_step_handler(message, reply_attack, defend_body_part=message.text)


def reply_attack(message, defend_body_part: str):
    if not BodyPart.include_item(message.text):
        bot.send_message(message.chat.id, "Необходимо выбрать вариант на клавиатуре!")
        game_next_step(message)
    else:
        attack_body_part = message.text

        global status

        user_naruto = status[message.chat.id]['user_naruto']
        naruto_npc = status[message.chat.id]['npc_naruto']

        user_naruto.next_step_p(BodyPart[attack_body_part],
                                BodyPart[defend_body_part])

        naruto_npc.next_step_p()

        game_step(message, user_naruto, naruto_npc)


def game_step(message, user_naruto: Naruto, naruto_npc: Naruto):
    comment_npc = naruto_npc.get_hit(enemy_att_point=user_naruto.attack_point,
                                     enemy_hit_pow=user_naruto.hit_power,
                                     enemy_type=user_naruto.naruto_type)
    bot.send_message(message.chat.id, f"NPC: {comment_npc}\nHP: {naruto_npc.hp}")

    comment_user = user_naruto.get_hit(enemy_att_point=naruto_npc.attack_point,
                                       enemy_hit_pow=naruto_npc.hit_power,
                                       enemy_type=naruto_npc.naruto_type)

    bot.send_message(message.chat.id, f"USER: {comment_user}\nHP: {user_naruto.hp}")

    if naruto_npc.state == NarutoStatus.READY and user_naruto.state == NarutoStatus.READY:
        bot.send_message(message.chat.id, "Продолжаем игру!")
        game_next_step(message)
    elif naruto_npc.state == NarutoStatus.DEFEATED and user_naruto.state == NarutoStatus.DEFEATED:
        bot.send_message(message.chat.id, "Ничья!")
        try:
            statistic_service.update_stat(message.chat.id, GameResult.E)
        except IndexError:
            bot.send_message(message.chat.id, "Не удалось обновить статистику :(")
    elif naruto_npc.state == NarutoStatus.DEFEATED:
        bot.send_message(message.chat.id, "Ты одержал победу!")
        try:
            statistic_service.update_stat(message.chat.id, GameResult.W)
        except IndexError:
            bot.send_message(message.chat.id, "Не удалось обновить статистику :(")
    elif user_naruto.state == NarutoStatus.DEFEATED:
        bot.send_message(message.chat.id, "Ты проиграл :(")
        try:
            statistic_service.update_stat(message.chat.id, GameResult.L)
        except IndexError:
            bot.send_message(message.chat.id, "Не удалось обновить статистику :(")


if __name__ == '__main__':
    print('Starting the bot...')
    bot.polling(none_stop=True, interval=0)
    print('The bot has stopped!')

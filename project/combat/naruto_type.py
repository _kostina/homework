from enum import Enum, auto


class NarutoType(Enum):

    WATER = auto()
    FIRE = auto()
    EARTH = auto()
    WIND = auto()
    LIGHTNING = auto()
    SAND = auto()
    WOOD = auto()
    SHADOW = auto()
    POISON = auto()
    EXPLOSION = auto()
    TAI = auto()

    @classmethod
    def min_value(cls):
        return cls.WATER.value

    @classmethod
    def max_value(cls):
        return cls.TAI.value






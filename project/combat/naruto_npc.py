import random

from project.combat.naruto import Naruto
from project.combat.naruto_type import NarutoType
from project.combat.naruto_by_type import naruto_by_type
from project.combat.body_part import BodyPart


class NarutoNPC(Naruto):
    def __init__(self):
        rand_type_value = random.randint(NarutoType.min_value(), NarutoType.max_value())
        rand_naruto_type = NarutoType(rand_type_value)

        rand_naruto_name = random.choice(list(naruto_by_type.get(rand_naruto_type).keys()))

        super().__init__(rand_naruto_name, rand_naruto_type)

    def next_step_p(self):
        attack_point = BodyPart(random.randint(BodyPart.min_value(), BodyPart.max_value()))
        defence_point = BodyPart(random.randint(BodyPart.min_value(), BodyPart.max_value()))
        super().next_step_p(next_att=attack_point,
                            next_defence=defence_point)


if __name__ == '__main__':
    naruto_npc = NarutoNPC()
    naruto_npc.next_step_p()
    print(naruto_npc)

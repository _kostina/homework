from project.combat.naruto_type import NarutoType
from project.combat.naruto_type_weakness import naruto_defence_weakness_by_type as weakness
from project.combat.body_part import BodyPart
from project.combat.naruto_status import NarutoStatus


class Naruto:
    def __init__(self, name: str,
                 naruto_type: NarutoType):
        self.name = name
        self.naruto_type = naruto_type
        self.weakness = weakness.get(naruto_type, tuple())
        self.hp = 150
        self.attack_point = None
        self.defence_point = None
        self.hit_power = 25
        self.state = NarutoStatus.READY

    def __str__(self):
        return f"Name: {self.name} | Type: {self.naruto_type.name}\nHP: {self.hp}"

    def next_step_p(self,
                    next_att: BodyPart,
                    next_defence: BodyPart):
        self.attack_point = next_att
        self.defence_point = next_defence

    def get_hit(self,
                enemy_att_point: BodyPart,
                enemy_hit_pow: int,
                enemy_type: NarutoType):
        if enemy_att_point == BodyPart.NOTHING:
            return "Техника маскировки тебя не подвела!"
        elif self.defence_point == enemy_att_point:
            return "Доходяга!"
        else:
            self.hp -= enemy_hit_pow * (4 if enemy_type in self.weakness else 1)

            if self.hp <= 0:
                self.state = NarutoStatus.DEFEATED
                return "Побежден!"
            else:
                return "Поврежден, но не сломлен!"


if __name__ == '__main__':
    n1 = Naruto('Гаара', NarutoType.SAND)
    print(n1)

from project.combat.naruto_type import NarutoType

naruto_by_type = {
    NarutoType.WATER: {'Kisame': 'Kisame.png',
                       'Suigetsu': 'Suigetsu.png'},

    NarutoType.FIRE: {'Itachi': 'Itachi.png',
                      'Jiraiya': 'Jiraiya.png'},

    NarutoType.EARTH: {'Kakashi': 'Kakashi.png',
                       'Oonoki': 'Oonoki.png'},

    NarutoType.WIND: {'Naruto': 'Naruto.png',
                      'Asuma': 'Asuma.png'},

    NarutoType.LIGHTNING: {'Sasuke': 'Sasuke.png',
                           'Ai': 'Ai.png'},

    NarutoType.SAND: {'Gaara': 'Gaara.png',
                      'Rasa': 'Rasa.jpg'},

    NarutoType.WOOD: {'Hoshirama': 'Hoshirama.jpg',
                      'Yamato': 'Yamato.png'},

    NarutoType.SHADOW: {'Shikaku': 'Shikaku.jpg',
                        'Shikamaru': 'Shikamaru.png'},

    NarutoType.EXPLOSION: {'Gari': 'Gari.png',
                           'Deidara': 'Deidara.png'},

    NarutoType.POISON: {'Sasori': 'Sasori.png',
                        'Kankuro': 'Kankuro.png'},

    NarutoType.TAI: {'Guy': 'Guy.png',
                     'Rock Lee': 'RockLee.png'},

}

from project.combat.naruto_type import NarutoType

naruto_defence_weakness_by_type = {
    NarutoType.WATER: (NarutoType.EARTH,
                       NarutoType.EXPLOSION),

    NarutoType.FIRE: (NarutoType.WATER,
                      NarutoType.SAND),

    NarutoType.EARTH: (NarutoType.LIGHTNING,
                       NarutoType.POISON),

    NarutoType.WIND: (NarutoType.FIRE,
                      NarutoType.EXPLOSION),

    NarutoType.LIGHTNING: (NarutoType.WIND,
                           NarutoType.EARTH),

    NarutoType.SAND: (NarutoType.WATER,
                      NarutoType.EXPLOSION),

    NarutoType.WOOD: (NarutoType.FIRE,
                      NarutoType.EXPLOSION,
                      NarutoType.TAI),

    NarutoType.SHADOW: (NarutoType.WIND,
                        NarutoType.EXPLOSION),

    NarutoType.EXPLOSION: (NarutoType.LIGHTNING,
                           NarutoType.EARTH),

    NarutoType.POISON: (NarutoType.WATER,),

    NarutoType.TAI: (NarutoType.SHADOW,
                     NarutoType.LIGHTNING)
}

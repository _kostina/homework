from project.combat.naruto import Naruto
from project.combat.naruto_status import NarutoStatus
from project.combat.naruto_type import NarutoType
from project.combat.naruto_type_weakness import naruto_defence_weakness_by_type as weakness_by_type


class TestNarutoClass:
    naruto_name = 'Naruto'
    naruto_type = NarutoType.WIND
    max_hp = 150

    def test_init(self):
        naruto_test = Naruto(name=self.__class__.naruto_name,
                             naruto_type=self.__class__.naruto_type)

        assert naruto_test.name == self.__class__.naruto_name
        assert naruto_test.naruto_type == self.__class__.naruto_type
        assert naruto_test.weakness == weakness_by_type[self.__class__.naruto_type]
        assert naruto_test.hp == self.__class__.max_hp
        assert naruto_test.attack_point is None
        assert naruto_test.defence_point is None
        assert naruto_test.hit_power == 25
        assert naruto_test.state == NarutoStatus.READY

    def test_str(self):
        naruto_test = Naruto(name=self.__class__.naruto_name,
                             naruto_type=self.__class__.naruto_type)
        assert str(naruto_test) == f"Name: {self.__class__.naruto_name} | " \
                                   f"Type: {self.__class__.naruto_type.name}\n" \
                                   f"HP: {self.__class__.max_hp}"

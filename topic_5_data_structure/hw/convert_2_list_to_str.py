def list_to_str(new_list, new_string):
    """
    Функция list_to_str.

    Принимает 2 аргумента: список и разделитель (строка).

    Возвращает (строку полученную разделением элементов списка разделителем,
    количество разделителей в получившейся строке в квадрате).

    Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.

    Если разделитель не является строкой, то возвращать строку 'Second arg must be str!'.

    Если список пуст, то возвращать пустой tuple().

    ВНИМАНИЕ: в списке могут быть элементы любого типа (нужно конвертировать в строку).
    """
    if type (new_list) != list:
        return 'First arg must be list!'
    elif type(new_string) != str:
        return 'Second arg must be str!'
    elif len(new_list) == 0:
        return tuple()
    new_str = new_string.join([str(i) for i in new_list])

    return new_str, (len(new_list)-1)**2

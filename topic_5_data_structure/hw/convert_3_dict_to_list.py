def dict_to_list(new_dict):
    """
    Функция dict_to_list.

    Принимает 1 аргумент: словарь.

    Возвращает кортеж: (
    список ключей,
    список значений,
    количество уникальных элементов в списке ключей,
    количество уникальных элементов в списке значений
    ).

    Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.
    """
    if type(new_dict) != dict:
        return 'Must be dict!'

    keys = list(new_dict.keys())
    values = list(new_dict.values())
    return keys, values, len(keys), len(set(values))
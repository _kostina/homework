def get_words_by_translation(eng_dict, word):
    """
    Функция get_words_by_translation.

    Принимает 2 аргумента:
    ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
    слово для поиска в словаре (eng).

    Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
    если нет, то ‘Can`t find English word: {word}’.

    Если вместо словаря передано что-то другое, то возвращать строку 'Dictionary must be dict!'.
    Если вместо строки для поиска передано что-то другое, то возвращать строку 'Word must be str!'.

    Если словарь пустой, то возвращать строку 'Dictionary is empty!'.
    Если строка для поиска пустая, то возвращать строку 'Word is empty!'.
    """
    if type(eng_dict) != dict:
        return 'Dictionary must be dict!'
    elif type(word) != str:
        return 'Word must be str!'

    if len(eng_dict) == 0:
        return 'Dictionary is empty!'
    elif len(word) == 0:
        return 'Word is empty!'

    translate = []

    for ru_word, eng_words in eng_dict.items():
       if word in eng_words:
           translate.append(ru_word)

    if len(translate) == 0:
        return f'Can`t find English word: {word}'

    return translate